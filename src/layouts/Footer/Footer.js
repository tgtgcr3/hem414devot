import React from "react";
import Newsletter from "../../component/Newsletter/Newsletter"
import FooterArea from "../../component/Footer/FooterArea";
import Copyrights from "../../component/Copyrights/Copyright";
import "../Footer/Footer.css"
function Footer() {

  const newsletter_title= "Sign Up Our Newsletter";
  const newsletter_subtitle= "We Offer An Informative Monthly Technology Newsletter - Check It Out.";
  const newsletter_submit= "Subscribe Now";
  const newsletter_placeholder= "Enter your email"

  const footer_heading1= "About Us";
  const footer_heading2= "Our Services";
  const footer_heading3= "Useful Links";
  const footer_heading4= "Contact Info";
  const footer_content= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco consectetur laboris.";
  const footer_textphone= "Phone";
  const footer_textemail= "Email";
  const footer_phone= "080 707 555-321";
  const footer_mail= "demo@example.com";
  const footer_address= "Address";
  const footer_textaddress= "526 Melrose Street, Water Mill, 11976 New York";

  const FooterLink1= [{
    title: "IT Solution",
    link: "#"
  },
  {
    title: "Web Development",
    link: "#"
  },
  {
    title: "Networking Services",
    link: "#"
  },
  {
    title: "SEO Optimization",
    link: "#"
  },
  {
    title: "App Optimization",
    link: "#"
  }];

  const FooterLink2= [{
    title: "About Us",
    link: "#"
  },
  {
    title: "Case Study",
    link: "#"
  },
  {
    title: "Contact Us",
    link: "#"
  },
  {
    title: "Privacy Policy",
    link: "#"
  },
  {
    title: "Terms & Conditions",
    link: "#"
  }];

  const copyright_content= "2021 Techvio - All Rights Reserved.";
  const terms_condition= "Terms & Conditions";
  const terms_condition_link= "#";
  const privacy_policy= "Privacy Policy";
  const privacy_policy_link= "#";
    return(
      <footer>
       <Newsletter 
              newsletter_title={newsletter_title}
              newsletter_subtitle={newsletter_subtitle}
              newsletter_placeholder={newsletter_placeholder}
              newsletter_submit={newsletter_submit}/>
        <FooterArea 
          footer_heading1={footer_heading1}
          footer_heading2={footer_heading2}
          footer_heading3={footer_heading3}
          footer_heading4={footer_heading4}
          footer_content={footer_content}
          footer_textphone={footer_textphone}
          footer_textemail={footer_textemail}
          footer_phone={footer_phone}
          footer_mail={footer_mail}
          footer_address={footer_address}
          footer_textaddress={footer_textaddress}
          FooterLink={FooterLink1}
          FooterLink2={FooterLink2}
         />
         <Copyrights 
          copyright_content={copyright_content}
          terms_condition={terms_condition}
          terms_condition_link={terms_condition_link}
          privacy_policy={privacy_policy}
          privacy_policy_link={privacy_policy_link}/>
      </footer >

    );
}
export default Footer;