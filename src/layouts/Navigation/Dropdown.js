import React from 'react';
import { NavLink } from "react-router-dom";

const Dropdown = ({ submenus }) => {
    return (
     <ul className="dropdown-menu">
      {submenus.map((submenu, index) => (
       <li key={index} className="nav-item">
        <NavLink to={submenu.link} className="nav-link">{submenu.title}</NavLink>
       </li>
      ))}
     </ul>
    );
   };
   
export default Dropdown;