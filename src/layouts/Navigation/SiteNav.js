import React, {useEffect, useState} from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';

import Dropdown from "../Navigation/Dropdown"

function SiteNav(){
    

   const menuItems = [
        //...
        {
         title: "Trang chủ",
         link: "/",
        //  submenu: [
        //   {
        //    title: "Home 1",
        //    link: "/"
        //   },
        //   {
        //     title: "Home 2",
        //     link: "/home2"
        //    }
        //  ]
        },
        {
            title: "About Us",
            link: "/about"
        },
        {
            title: "Services",
            link: "",
            submenu: [
             {
              title: "Services",
              link: "/services"
             },
             {
               title: "Services Detail",
               link: "/services-detail"
              }
            ]
        },
        {
            title: "Project",
            link: "",
            submenu: [
             {
              title: "Project",
              link: "/project"
             },
             {
               title: "Project Detail",
               link: "/project-detail"
              }
            ]
        },
        {
            title: "Page",
            link: "",
            submenu: [
                {
                    title: "About Us",
                    link: "#"
                },
                {
                    title: "Team",
                    link: "#"
                },
                {
                    title: "Pricing",
                    link: "#"
                },
                {
                    title: "404 Error",
                    link: "#"
                },
                {
                    title: "FAQ",
                    link: "#"
                },
                {
                    title: "Coming Soon",
                    link: "#"
                },
                {
                    title: "Terms &amp; Conditions",
                    link: "#"
                }
            ]
        },
        {
            title: "Blog",
            link: "",
            submenu: [
             {
              title: "Blog",
              link: "/blog"
             },
             {
               title: "Blog Right Sidebar",
               link: "#"
             },
             {
               title: "Blog Detail",
               link: "#"
              }
            ]
        },
        {
            title: "Contact",
            link: "/contact"
        },
        //...
       ];
    const [showDropdown, setShowDropdown] = useState(false)
    const [title, setTitle] = useState("")

    useEffect(() => {
        if (showDropdown !== false) {
            // console.log(`The name is now... ${showDropdown}!`)
        }
        }, [showDropdown])
        
        function handleRun(TitleDrop) {
        // const divElement = navRef.current;
        // console.log(divElement);
        setTitle(title => TitleDrop)
        var element = document.getElementsByClassName('nav-item-sub');
        // Printing the element
        if (window.innerWidth < 991 ){
            if (showDropdown === true && element ){
                // setShowDropdown(false)
                setShowDropdown(showDropdown => !showDropdown)
            }else {
                // setShowDropdown(true)
                setShowDropdown(showDropdown => !showDropdown)
    
            }
        }
    }
  return (
    <>
        <ul className="navbar-nav">
            {menuItems.map((items, index) => {
                return (
                    <li key={index} className={`nav-item nav-item-sub ${index} ${showDropdown && items.title === title ? ('active'):('')}`} onClick={() => handleRun(items.title)}>
                        {items.submenu ? (
                        <>
                            <a href="#" className={`nav-link `} type="button" aria-haspopup="menu" >
                            {items.title}{" "}
                            <FontAwesomeIcon icon={faAngleDown} />
                            </a>
                            <Dropdown submenus={items.submenu} />
                            {showDropdown && items.title === title ? (
                               <a className="mean-expand mean-clicked" href="#" style={{fontSize: 18}}>-</a>
                            ):(<a className="mean-expand" href="#" style={{fontSize: 18}}>+</a>)}
                            
                        </>
                    ) : (
                        <NavLink to={items.link} className="nav-link">{items.title}</NavLink>
                    )}
                    </li>
                )
            })}
        </ul>
    </>
  );
}

export default SiteNav;


