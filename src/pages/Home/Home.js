import React from "react";
import HeroBanners from "../../component/Hero-banner/Hero-banner"
import Services from "../../component/Services/Service"
import Overview from "../../component/Overview/Overview"
import Counter from "../../component/Counter/Counter"
import Testimonial from "../../component/Testimonial/Testimonial"
import Team from "../../component/Team/Team"
import Brand from "../../component/Brand/Brand"
import "../Home/Home.css"

function Homepage() {

    let body
    body = (
        <>
            <HeroBanners
                index="1"
            />
            <Services
                index="1"
            />
            <Overview
                index="1"
            />
            {/* <Overview 
                 index = "2"
                />     */}
            <Counter
                index="1"
            />
            <Testimonial
                index="1"
            />
            <Team
                index="1"
            />
            <HeroBanners
                index="2"
            />
            <Brand
                index="1"
            />
        </>

    )

    return (<>
        {body}
    </>
    )
}

export default Homepage;