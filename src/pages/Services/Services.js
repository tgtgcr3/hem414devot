import React from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import Services from "../../component/Services/Service";
import Overview from "../../component/Overview/Overview";
import HeroBanners from "../../component/Hero-banner/Hero-banner";



function ServicesPage() {


    let body
    
        body = (
            <>
                <HeaderPage
                    index="1"
                />
                <Services
                    index="1"
                />
                <HeroBanners
                    index="1"
                />
                <Overview
                    index="1"
                />
                <Overview
                    index="2"
                />
            </>
        )
    return (
        <>
            {body}
        </>
    )
}

export default ServicesPage;