import React from "react";


function Admin () {
    const Page = [{
        id: "1",
        name: "home"
    },
    {
        id: "2",
        name: "about"
    },
    {
        id: "3",
        name: "services"
    },
    {
        id: "4",
        name: "services-detail"
    },
    ]
    
  return (
    <>
    <section>
        <ul className="nav">
            {Page.map((todo, index) => {
                return(
                    <li id={todo.name} key={index} className="default-btn" data-group={todo.name}>{todo.name}</li>
                )
            })}
        </ul>
    </section>
    </>
  );
}

export default Admin;
