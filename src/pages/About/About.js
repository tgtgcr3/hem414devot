import React from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import About from "../../component/About/About";
import Counter from "../../component/Counter/Counter";
import Team from "../../component/Team/Team";
import Testimonial from "../../component/Testimonial/Testimonial";
import HeroBanners from "../../component/Hero-banner/Hero-banner";
import Brand from "../../component/Brand/Brand";


function AboutPage() {

    let body

    body = (
        <>
            <HeaderPage
                index="1" />
            <About
                index="1" />
            <Counter
                index="1" />
            <Team
                index="1" />
            <Testimonial
                index="1" />
            <HeroBanners
                index="2"
            />
            <Brand
                index="1"
            />
        </>
    )

    return (
        <>
            {body}
        </>
    )
}

export default AboutPage;