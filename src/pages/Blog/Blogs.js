import React , {useEffect} from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import Blog from "../../component/Blog/Blog";
import { BlogPageSelector, getBlogPage } from '../../store/reducersrenew/BlogPageReducers'
import { useSelector, useDispatch } from 'react-redux'
import Spinner from 'react-bootstrap/Spinner'


function BlogPage() {
    const dispatch = useDispatch()
    const BlogPageSelectors = useSelector(BlogPageSelector)

    useEffect(() => {
        dispatch(getBlogPage())
    }, [dispatch])

    let body
    if (BlogPageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <>
                <HeaderPage
                    index="1"
                />
                <Blog
                    index="1"
                />
            </>)
    }
    return (
        <>
            {body}
        </>
    )
}

export default BlogPage;