import { createContext, useReducer, useState } from 'react'


export const PublicContext = createContext()

const PublicContextProvider = ({ children }) => {

	const [isLoading, setIsLoading] = useState(false)
	// Post context data
	const publicContextData = {
		isLoading, 
		setIsLoading
	}

	return (
		<PublicContext.Provider value={publicContextData}>
			{children}
		</PublicContext.Provider>
	)
}

export default PublicContextProvider