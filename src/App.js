import "../src/App.css"
import Layout from '../src/layouts/Content/Layout'
import { BannerSelector, getBanner, } from './store/reducersrenew/BannerReducers'
import Spinner from 'react-bootstrap/Spinner'
import React, { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux'
import Loading from './component/Loading/Loading'
function App() {
    const dispatch = useDispatch()
    const BannerSelectors = useSelector(BannerSelector)
    useEffect(() => {
        dispatch(getBanner())
    }, [dispatch])

    let body

    if (BannerSelectors.isLoading) {
        body = (
            // <div className='spinner-container'>
            //     <Spinner animation='border' variant='info' />
            // </div>
            <div className='Loading'>
                <Loading></Loading>
            </div>

        )
    }
    else {
        body = (
            <>
                <Layout>
                </Layout>
            </>

        )
    }
    return (
        <>
            {body}

        </>
    )
}

export default App;