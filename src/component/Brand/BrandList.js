import React from "react";
import { useSelector} from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'
function BrandList({ParentPage, index}){
    const id_page = 'BrandList'; 

    const todosSelector = useSelector(BannerSelector)

    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)


  return (
      <>
        {todosLoading.map((todo, index) => {
            return (
                <div key={index} className="partner-item">
                    <a href={todo.link_01}>
                        <img src={todo.image_url_01} alt={todo.image_url_01}/>
                    </a>
                </div>
            );
        })}
    </>
  );
}

export default BrandList;
