import React from "react";
import { useSelector } from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'
import BrandList from "../Brand/BrandList"
import "../Brand/Brand.css"
import { useLocation } from 'react-router-dom'


function Brand({index}){
    const id_page = 'Brand'; 
	let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");
    const todosSelector = useSelector(BannerSelector)
  
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)

  return (
    <section className="partner-section pt-100 pb-70">
		<div className="container">
		{todosLoading.map((todo, index) => {
        return (
			<div key={index}className="partner-title">
				<h6>{todo.title_01}</h6>
				<h2>{todo.subtitle_01}</h2>
			</div>
		)})}
			<div className="partner-list">
				<BrandList ParentPage={ParentPage} index="1"/>
			</div>
		</div>
	</section>
  );
}

export default Brand;