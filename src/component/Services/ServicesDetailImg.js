import React from "react";
import { useSelector } from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'
import Slider from "react-slick";


function ServicesDetailListImg({ParentPage, index}){
    
    const id_page = 'ServicesDetailListImg';

    const todosSelector = useSelector(BannerSelector)

    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)

    const settings = {
        className: 'image-sliders',
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        speed: 500,
        autoplay:true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
      };
  return (
      <>
        <Slider {...settings}>
            {todosLoading.map((todo, index) => {
                return (
                    <div key={index} className="services-details-image">
                        <img src={todo.image_url_01} alt="image" />
                    </div>
                );
            })}
        </Slider>
    </>
  );
}

export default ServicesDetailListImg;


