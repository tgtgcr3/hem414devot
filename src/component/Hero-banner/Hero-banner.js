import React from "react";
import { useSelector } from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'
import "../Hero-banner/Hero-banner.css"
import { useLocation } from 'react-router-dom'

function HeroBanners({ index }) { 
    const id_page = 'HeroBanners';

    let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");
    const todosSelector= useSelector(BannerSelector)
    
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)
    let body
    body = (
        <>
            {todosLoading.map(todo => (
                <div key={index} className={`home-area ${todo.classname_01}`} style={todo.classname_01 == 'style_2' ? { backgroundImage: `url(${todo.background})` } : {}}>
                    <div className="d-table">
                        <div className="d-table-cell">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-lg-6 col-md-12">
                                        <div className="main-banner-content">
                                            <h6>{todo.subtitle_01}</h6>
                                            <h1>{todo.title_01}</h1>
                                            <p>{todo.content_01}</p>
                                            <div className="banner-btn">
                                                <a href={todo.link_01} className="default-btn-one">
                                                    {todo.button_01}
                                                    <span></span>
                                                </a>
                                                <a className="default-btn" href={todo.link_02}>
                                                    {todo.button_02}
                                                    <span></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    {todo.classname_01 == 'style_1' ? (
                                        <div className="col-lg-6 col-md-12">
                                            <div className="banner-image">
                                                <img src={todo.image_url_01} alt="image" />
                                            </div>
                                        </div>) : ('')}

                                </div>
                            </div>
                        </div>
                    </div>
                    {todo.classname_01 == 'style_1' ? (
                        <div className="creative-shape">
                            <img src={todo.image_url_02} alt="svg shape" />
                        </div>) : ('')}
                </div>
            ))}
        </>
    )


    return (
        <>
            {body}
        </>
    )
}
export default HeroBanners

