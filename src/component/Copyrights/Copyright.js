import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopyright } from '@fortawesome/free-solid-svg-icons';

function Copyrights(props){
  return (
    <div className="copyright-area">
		<div className="container">
			<div className="row align-items-center">
				<div className="col-lg-6 col-md-6">
					<p> <FontAwesomeIcon icon={faCopyright} /> {props.copyright_content}</p>
				</div>
				<div className="col-lg-6 col-md-6">
					<ul>
						<li> <Link to={props.terms_condition_link}>{props.terms_condition}</Link>
						</li>
						<li> <Link to={props.privacy_policy_link}>{props.privacy_policy}</Link>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
  );
}

export default Copyrights;