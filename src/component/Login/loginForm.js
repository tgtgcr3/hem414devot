import React, { useContext, useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import AlertMessage from '../../layouts/AlertMessage/AlertMessage'
import { useSelector, useDispatch } from 'react-redux'
import './LoginForm.css'
import {
  UsersSelector,
  getUsers,
} from '../../store/reducers/UserReducers'

const LoginForm = () => {
  //Context
  const dispatch = useDispatch()
  const todosUsersSelector = useSelector(UsersSelector)
  //Local state
  const [loginForm, setLoginForm] = useState({
    username: '',
    password: ''
  })

  const [alert, setAlert] = useState(null)


  const { username, password } = loginForm
  const onChangeLoginForm = event => setLoginForm({ ...loginForm, [event.target.name]: event.target.value })
  const login = async event => {
    event.preventDefault()

    try {
      const loginData = dispatch(getUsers(loginForm))
      // if (loginData.success) {
      //   // history('../Dashboard')
      // }
      // else {
      //   setAlert({ type: 'danger', message: loginData.message })
      //   setTimeout(() => {
      //     setAlert(null)
      //   }, 5000);
      // }
    }
    catch (error) {
      console.log(error)
    }

  }
  return (
    <>
      <div className='landing'>
        <div className='dark-overlay'>
          <div className='landing-inner'>
            <h1>Đăng nhập</h1>
            <Form className='my-4' onSubmit={login}>
              <AlertMessage info={alert} />
              <Form.Group className='FormGroup'>
                <Form.Control type='text' placeholder='Username' name='username' required value={username} onChange={onChangeLoginForm}></Form.Control>
              </Form.Group>
              <Form.Group className='FormGroup'>
                <Form.Control type='password' placeholder='Password' name='password' required value={password} onChange={onChangeLoginForm}></Form.Control>
              </Form.Group>
              <Button variant='success' type='submut'>Login</Button>
            </Form>
          </div>
        </div>
      </div>

    </>
  )
}

export default LoginForm