import React from "react";
import { Link } from "react-router-dom";

function FooterLink(props){
  return (
      <>
        {props.FooterLink.map((FooterLink, index) => {
            return (
            <li key={index}> <Link to={FooterLink.link}>{FooterLink.title}</Link></li>
            );
        })}
    </>
  );
}

export default FooterLink;
