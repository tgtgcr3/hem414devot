import React, {useEffect} from "react";
import { useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import {
	MapSelector,
	getMap,
} from '../../store/reducers/MapReducer'
import "../Contact/Map.css"
// import GoogleMapReact from 'google-maps-react';

// const AnyReactComponent = ({ text }) => <div>{text}</div>;

function Map({index}){

	let location = useLocation();
    const id_page = location.pathname.replace("/","").replace("/","");
    let Map = useSelector(MapSelector)
    Map = index === ''?Map:Map.filter(todo => todo.index === index && todo.id_page === id_page)
    const dispatch = useDispatch()
    useEffect(() => {
        // send request to jsonplaceholder
        dispatch(getMap())

    }, [dispatch])
  return (
	  <>
    <div className="map-area">
        <div className="map-content">
                     <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.521260322283!2d106.8195613507864!3d-6.194741395493371!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5390917b759%3A0x6b45e67356080477!2sPT%20Kulkul%20Teknologi%20Internasional!5e0!3m2!1sen!2sid!4v1601138221085!5m2!1sen!2sid"
              width="100%"
              height="450"
              frameBorder="0"
              style={{ border: 0 }}
              allowFullScreen=""
              aria-hidden="false"
              tabIndex="0"
            />
            {/* {Map.map((todo, index) => {
                return (
                    <div key={index}>{todo.map}
                        <GoogleMapReact
                        bootstrapURLKeys={{ key: "AIzaSyCHk1k1Xj_gzxjh10jN873yGvzxxF6qXbI_ji" }}
                        defaultCenter={{ lat: -25.363, lng: 131.044 }}
                        defaultZoom={8}
                        >
                        <AnyReactComponent
                        lat={-25.363}
                        lng={131.044}
                        text="My Marker"
                        />
                        </GoogleMapReact>
                    </div>
                );
            })} */}
        </div>
    </div>
	</>
  );
}

export default Map;