import React, {useEffect, useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp } from '@fortawesome/free-solid-svg-icons';

function BackToTop() {
    const [showGoToTop, setShowGoToTop] = useState(false)
    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY >= 200) {
                setShowGoToTop(true)
            } else {
                setShowGoToTop(false)
            }
        }

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        }
    }, [])


    return (
        <>
            <div className={`go-top  ${showGoToTop ? 'active' : ''}`}>
            <FontAwesomeIcon icon={faAngleUp} />  
            </div>       
        </>
    )
}
export default BackToTop;