import React from "react";
import "../Newsletter/Newsletter.css"

function Newsletter(props){
  return (
	<section className="footer-subscribe-wrapper">
    	<div className="subscribe-area">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-lg-6 col-md-6">
						<div className="subscribe-content">
							<h2>{props.newsletter_title}</h2>
							<span className="sub-title">{props.newsletter_subtitle}</span>
						</div>
					</div>
					<div className="col-lg-6 col-md-6">
						<form className="newsletter-form">
							<input type="email" className="input-newsletter" placeholder={props.newsletter_placeholder} name="EMAIL" required=""/>
							<button type="submit">{props.newsletter_submit}</button>
							<div id="validator-newsletter" className="form-result"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
  );
}

export default Newsletter;