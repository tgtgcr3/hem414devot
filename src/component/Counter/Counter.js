import React from "react";
import CounterList from "../Counter/CounterList"
import "../Counter/Counter.css"
import { useLocation } from 'react-router-dom'

function Counter({index}){
  const id_page = 'Counter'; 
  let location = useLocation();
  const ParentPage = location.pathname.replace("/", "").replace("/", "");

  return (
    <section className="counter-area section-padding">
		<div className="container">
			<div className="row">
                <CounterList ParentPage={ParentPage} index="1"/>
			</div>
		</div>
	</section>
  );
}

export default Counter;
