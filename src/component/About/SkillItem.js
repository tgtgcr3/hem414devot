import React from "react";
import { useSelector } from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'

function SkillItem({ ParentPage, index }) {
  const id_page = 'SkillItem';
  const todosSelector = useSelector(BannerSelector)

  let todosLoading = todosSelector.allPageLoad
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)

  return (
    <div>
      {todosLoading.map((todo, index) => {
        return (
          <div key={index} className="skill-item">
            <h6>{todo.title_01} <em>{todo.title_02}</em></h6>
            <div className="skill-progress">
              <div className="progres" data-value={todo.title_02} style={{ width: `${todo.title_02}` }}></div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default SkillItem;

