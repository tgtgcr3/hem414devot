import React from "react";
import { useSelector } from 'react-redux'
import { BannerSelector } from '../../store/reducersrenew/BannerReducers'

function FeaturesList({ ParentPage, index }) {
  const id_page = 'FeaturesList';
  const todosSelector = useSelector(BannerSelector)
  
  let todosLoading = todosSelector.allPageLoad
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.pathtype === ParentPage)

  return (
    <ul className="features-list">
      {todosLoading.map((todo, index) => {
        return (
          <li key={index}> <span>{todo.title_01}</span></li>
        );
      })}
    </ul>
  );
}

export default FeaturesList;

