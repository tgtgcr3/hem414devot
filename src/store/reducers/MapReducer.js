import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getMap = createAsyncThunk('Map/todosFetched', async() => {
    const response = collection(db, 'Map');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const MapSlice = createSlice({
    name: 'Map',
    initialState: {
        allMap: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getMap.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getMap.fulfilled]: (state, action) => {
            console.log('Done')
            state.allMap = action.payload
        },
        [getMap.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const MapReducer = MapSlice.reducer

// Selector
export const MapSelector = state => state.MapReducer.allMap

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = MapSlice.actions

// Export reducer
export default MapReducer