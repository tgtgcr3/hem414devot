import { configureStore } from '@reduxjs/toolkit'

import MapReducer from './reducers/MapReducer'
import UsersReducer from './reducers/UserReducers'
import HomePageReducer from './reducersrenew/HomePageReducers'
import AboutPageReducer from './reducersrenew/AboutReducers'
import ServicesPageReducer from './reducersrenew/ServicesReducers'
import ServiceDetailPageReducer from './reducersrenew/ServiceDetailReducers'
import ProjectPageReducer from './reducersrenew/ProjectPageReducers'
import BlogPageReducer from './reducersrenew/BlogPageReducers'
import ContactPageReducer from './reducersrenew/ContactPageReducers'

import BannerReducer from './reducersrenew/BannerReducers'
// Store
const store = configureStore({
    reducer: {
        MapReducer,
        UsersReducer,
        HomePageReducer,
        ServicesPageReducer,
        ServiceDetailPageReducer,
        ProjectPageReducer,
        BlogPageReducer,
        ContactPageReducer,
        AboutPageReducer,
        BannerReducer
    }
});

// Export
export { store }