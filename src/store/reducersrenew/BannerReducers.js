import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBanner = createAsyncThunk('Banner/todosFetched', async() => {
    const response = collection(db, "Banner_Vi");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const BannerSlice = createSlice({
    name: 'Banner',
    initialState: {
        allPageLoad: [],
        isLoading: true,
        project_name: 'all'
    },
    reducers: {
        findprojectname(state, action) {
            const postID = action.payload
            state.project_name = postID
        }
    },
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBanner.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getBanner.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getBanner.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const BannerReducer = BannerSlice.reducer

// Selector
export const BannerSelector = state => state.BannerReducer

// Action export
export const {
    findprojectname
    // deleteTodo
    // todosFetched
} = BannerSlice.actions

// Export reducer
export default BannerReducer