import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getAboutPage = createAsyncThunk('AboutPage/todosFetched', async() => {
    const response = collection(db, "AboutPage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const AboutPageSlice = createSlice({
    name: 'AboutPage',
    initialState: {
        allPageLoad: [],
        isLoading: true
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getAboutPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getAboutPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getAboutPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const AboutPageReducer = AboutPageSlice.reducer

// Selector
export const AboutPageSelector = state => state.AboutPageReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = AboutPageSlice.actions

// Export reducer
export default AboutPageReducer