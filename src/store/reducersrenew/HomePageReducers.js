import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getHomePage = createAsyncThunk('HomePage/todosFetched', async() => {
    const response = collection(db, "HomePage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const HomePageSlice = createSlice({
    name: 'HomePage',
    initialState: {
        allPageLoad: [],
        isLoading: true
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getHomePage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getHomePage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getHomePage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const HomePageReducer = HomePageSlice.reducer

// Selector
export const HomePageSelector = state => state.HomePageReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = HomePageSlice.actions

// Export reducer
export default HomePageReducer