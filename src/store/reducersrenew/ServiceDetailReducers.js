import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServiceDetailPage = createAsyncThunk('ServiceDetailPage/todosFetched', async() => {
    const response = collection(db, "ServicesDetailPage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServiceDetailPageSlice = createSlice({
    name: 'ServiceDetailPage',
    initialState: {
        allPageLoad: [],
        isLoading: true
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getServiceDetailPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getServiceDetailPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getServiceDetailPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const ServiceDetailPageReducer = ServiceDetailPageSlice.reducer

// Selector
export const ServiceDetailPageSelector = state => state.ServiceDetailPageReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServiceDetailPageSlice.actions

// Export reducer
export default ServiceDetailPageReducer