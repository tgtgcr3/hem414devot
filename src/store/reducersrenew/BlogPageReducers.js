import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBlogPage = createAsyncThunk('BlogPage/todosFetched', async() => {
    const response = collection(db, "BlogPage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const BlogPageSlice = createSlice({
    name: 'BlogPage',
    initialState: {
        allPageLoad: [],
        isLoading: true
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBlogPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getBlogPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getBlogPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const BlogPageReducer = BlogPageSlice.reducer

// Selector
export const BlogPageSelector = state => state.BlogPageReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = BlogPageSlice.actions

// Export reducer
export default BlogPageReducer