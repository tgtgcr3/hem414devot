import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServicesPage = createAsyncThunk('ServicesPage/todosFetched', async() => {
    const response = collection(db, "ServicesPage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServicesPageSlice = createSlice({
    name: 'ServicesPage',
    initialState: {
        allPageLoad: [],
        isLoading: true
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getServicesPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getServicesPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getServicesPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const ServicesPageReducer = ServicesPageSlice.reducer

// Selector
export const ServicesPageSelector = state => state.ServicesPageReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServicesPageSlice.actions

// Export reducer
export default ServicesPageReducer